# Test ansible

App: <https://github.com/readthedocs-fr/bin-server>

## Commands

```sh
ansible-galaxy init --init-path=roles nginx
ansible-lint roles/

ansible-playbook site.yaml -K
```
